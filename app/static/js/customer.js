var datatable;
var data;

$(document).ready(function () {
	datatable = $("#table").DataTable();
	getAllData();
});

function getAllData() {
	$.ajax({
		url: "get-all-customer",
		method: "GET",
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			data = respuesta;
			datatable.clear().draw();
			respuesta.forEach((element,index) => {
				let tipo_documento = element.tipo_documento_id == 1 ? "DNI" : element.tipo_documento_id == 2 ? "RUC" : "OTRO";
				datatable.row.add([
					index + 1,
					element.nombre_completo,
					tipo_documento,
					element.documento_identidad,
					element.email,
					element.direccion,
					`<button class="btn btn-primary" onclick="openModal(${element.id})">
						<i class="fas fa-edit text-white"></i>
					</button>
					<button class="btn btn-danger" onclick="confirmDelete(${element.id})">
						<i class="fas fa-trash text-white"></i>
					</button>`,
				]).draw(false);
			});
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function confirmDelete(id) {

	$("#customer").trigger("reset");
	$("#customer").find('input[name="id"]').val(id);
	setTimeout(() => {
		this.sendData(id);
	}, 500);

}

function sendData(eliminar = null) {
	if (!validateForm() && eliminar === null) {
		return;
	}
	let data = getFormData("customer");
	data['eliminar'] = eliminar;
	data['tipo_documento_id'] = $("#customer").find('select[name="tipo_documento_id"]').val();

	$.ajax({
		url: "customer",
		method: "POST",
		data: data,
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			mostrarAlerta("success", "Se ejecuto correctamente");
			getAllData();
			$("#modal").modal("hide");
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function openModal(id = null) {
	if (id === null) {
		clearForm();
		$("#modal").modal("show");
		return;
	}

	let item = data.find((element) => element.id == id);
	$("#modal-title").text("Editar Cliente");
	$("#modal-btn").text("Editar");
	$("#customer").trigger("reset");
	$("#customer").find('input[name="nombre_completo"]').val(item.nombre_completo);
	$("#customer").find('select[name="tipo_documento_id"]').val(item.tipo_documento_id);
	$("#customer").find('input[name="documento_identidad"]').val(item.documento_identidad);
	$("#customer").find('input[name="email"]').val(item.email);
	$("#customer").find('input[name="direccion"]').val(item.direccion);
	$("#customer").find('input[name="id"]').val(item.id);
	$("#modal").modal("show");
}

function validateForm() {
	let nombre_completo = $("#customer").find('input[name="nombre_completo"]').val();
	let tipo_documento_id = $("#customer").find('select[name="tipo_documento_id"]').val();
	let documento_identidad = $("#customer").find('input[name="documento_identidad"]').val();
	let email = $("#customer").find('input[name="email"]').val();
	let direccion = $("#customer").find('input[name="direccion"]').val();

	if (nombre_completo === "") {
		mostrarAlerta("error", "El nombre es requerido");
		return false;
	}

	if (tipo_documento_id === "") {
		mostrarAlerta("error", "El tipo de documento es requerido");
		return false;
	}

	if (documento_identidad === "") {
		mostrarAlerta("error", "El documento de identidad es requerido");
		return false;
	}

	if (email === "") {
		mostrarAlerta("error", "El email es requerido");
		return false;
	}

	if (direccion === "") {
		mostrarAlerta("error", "La direccion es requerida");
		return false;
	}

	return true;

}

function clearForm() {
	$("#customer").trigger("reset");
	$("#modal-title").text("Agregar Cliente");
}
