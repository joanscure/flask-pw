var datatable;
var data;

$(document).ready(function () {
	datatable = $("#table").DataTable();
	getAllData();
});

function getAllData() {
	$.ajax({
		url: "get-all-products",
		method: "GET",
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			data = respuesta;
			datatable.clear().draw();
			data.forEach((element, index) => {
				datatable.row
					.add([
						index + 1,
						element.nombre,
						element.precio,
						element.stock,
						element.stock_minimo,
						`<button class="btn btn-warning" onclick="openModal(${element.id})">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button class="btn btn-danger" onclick="sendData(${element.id})">
                        <i class="fas fa-trash"></i>
                    </button>`,
					])
					.draw(false);
			});
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function confirmDelete(id) {
	$("#product").trigger("reset");
	$("#product").find('input[name="id"]').val(id);
	setTimeout(() => {
		this.sendData(id);
	}, 500);
}

function sendData(eliminar = null) {
	if (!validateForm() && eliminar === null) {
		return;
	}
	let data = getFormData("product");
	data['eliminar'] = eliminar;


	$.ajax({
		url: "product",
		method: "POST",
		data: data,
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			mostrarAlerta("success", "Se ejecuto correctamente");
			getAllData();
			$("#modal").modal("hide");
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function openModal(id = null) {
	if (id === null) {
		clearForm();
		$("#modal").modal("show");
		return;
	}

	let item = data.find((element) => element.id == id);
	$("#modal-title").text("Editar Producto");
	$("#modal-btn").text("Editar");
	$("#product").trigger("reset");
	$("#product").find('input[name="nombre"]').val(item.nombre);
	$("#product").find('input[name="precio"]').val(item.precio);
	$("#product").find('input[name="stock"]').val(item.stock);
	$("#product").find('input[name="stock_minimo"]').val(item.stock_minimo);
	$("#product").find('input[name="id"]').val(item.id);
	$("#modal").modal("show");
}

function validateForm() {
	let nombre = $("#product").find('input[name="nombre"]').val();
	let precio = $("#product").find('input[name="precio"]').val();
	let stock = $("#product").find('input[name="stock"]').val();
	let stock_minimo = $("#product").find('input[name="stock_minimo"]').val();

	if (nombre === "") {
		mostrarAlerta("error", "El nombre es obligatorio");
		return false;
	}
	if (precio === "") {
		mostrarAlerta("error", "El precio es obligatorio");
		return false;
	}
	if (stock === "") {
		mostrarAlerta("error", "El stock es obligatorio");
		return false;
	}
	if (stock_minimo === "") {
		mostrarAlerta("error", "El stock minimo es obligatorio");
		return false;
	}

	return true;
}

function clearForm() {
	$("#product").find('input[name="nombre"]').val("");
	$("#product").find('input[name="precio"]').val("");
	$("#product").find('input[name="stock"]').val("");
	$("#product").find('input[name="stock_minimo"]').val();
	$("#product").find('input[name="id"]').val("");
}
