var datatable;
var data;

$(document).ready(function () {
	datatable = $("#table").DataTable();
	getAllData();
});

function getAllData() {
	$.ajax({
		url: "get-all-users",
		method: "GET",
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			data = respuesta;
			datatable.clear().draw();
			data.forEach((element, index) => {
				datatable.row
					.add([
						index + 1,
						element.nombre,
						element.clave,
						element.email,
						`<button class="btn btn-warning" onclick="openModal(${element.id})">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button class="btn btn-danger" onclick="sendData(${element.id})">
                        <i class="fas fa-trash"></i>
                    </button>`,
					])
					.draw(false);
			});
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function confirmDelete(id) {

	$("#user").trigger("reset");
	$("#user").find('input[name="id"]').val(id);
	setTimeout(() => {
		this.sendData(id);
	}, 500);

}

function sendData(eliminar = null) {
	if (!validateForm() && eliminar === null) {
		return;
	}
	let data = getFormData("user");
	data['eliminar'] = eliminar;


	$.ajax({
		url: "user",
		method: "POST",
		data: data,
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			mostrarAlerta("success", "Se ejecuto correctamente");
			getAllData();
			$("#modal").modal("hide");
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}

function openModal(id = null) {
	if (id === null) {
		clearForm();
		$("#modal").modal("show");
		return;
	}

	let item = data.find((element) => element.id == id);
	$("#modal-title").text("Editar Usuario");
	$("#modal-btn").text("Editar");
	$("#user").trigger("reset");
	$("#user").find('input[name="nombre"]').val(item.nombre);
	$("#user").find('input[name="clave"]').val(item.clave);
	$("#user").find('input[name="email"]').val(item.email);
	$("#user").find('input[name="id"]').val(item.id);
	$("#modal").modal("show");
}

function validateForm() {
	let nombre = $("#user").find('input[name="nombre"]').val();
	let clave = $("#user").find('input[name="clave"]').val();
	let email = $("#user").find('input[name="email"]').val();

	if (nombre === "") {
		mostrarAlerta("error", "El nombre es requerido");
		return false;
	}
	if (clave === "") {
		mostrarAlerta("error", "La clave es requerida");
		return false;
	}
	if (email === "") {
		mostrarAlerta("error", "El email es requerido");
		return false;
	}
	return true;

}

function clearForm() {
	$("#user").find('input[name="nombre"]').val("");
	$("#user").find('input[name="clave"]').val("");
	$("#user").find('input[name="email"]').val("");
	$("#user").find('input[name="id"]').val("");
}
