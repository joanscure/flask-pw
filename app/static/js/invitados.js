function nuevoInvitados() {
    $("#idinvitados").val("");
    $("#idinvitados").hide();
    $("#btn-buscar").hide();
    $("#btn-guardar").removeAttr("disabled");
    $("#btn-nuevo").attr("disabled", true);
}

function buscarInvitados() {
    var idinvitados = $("#idinvitados").val();
    if (idinvitados != "") {
        $.ajax({
            url: "invitados/" + idinvitados,
            method: 'GET',
            data: {},
            cache: false,
            dataType: 'json',
            success: function (respuesta) {
                if(respuesta.status=='404')
                {
                    mostrarAlerta('error','El Invitado no se encontró');
                }else{
                    var invitados = respuesta;
                    $("#idinvitados").attr("readonly", true);
                    $("#btn-buscar").attr("disabled", true);

                    $("#nombre").val(invitados.nombre);
                    $("#apellidos").val(invitados.apellidos);
                    $("#direccion").val(invitados.direccion);
                    $("#celular").val(invitados.celular);
                    $("#email").val(invitados.email);
                    $("#ideventos").val(invitados.ideventos);
                    $("#idinstituciones").val(invitados.idinstituciones);
                    $("#asistencia1").attr('checked', invitados.asistencia);
                    $("#asistencia2").attr('checked', !invitados.asistencia);

                    $("#btn-guardar").removeAttr("disabled");
                    $("#btn-eliminar").removeAttr("disabled");
                    $("#btn-nuevo").attr("disabled", true);
                }

            },
            error: function () {
                mostrarAlerta('error',"Ocurrio un error inesperado");
            }
        });
    } else {
        mostrarAlerta('error',"Ingrese un codigo");
    }
}
function eliminarInvitados()
{
    $("[name='user-form']").append('<input type="hidden" name="eliminar" value="true">');
}

function salir() {
    window.location.href = "./invitados";
}
