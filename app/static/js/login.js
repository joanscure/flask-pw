function login() {
	if (nombre.value != "" && clave.value != "") {
		$.ajax({
			url: "login",
			method: "POST",
			data: { nombre: nombre.value, clave: clave.value },
			cache: false,
			dataType: "json",
			success: function (respuesta) {
				if(respuesta){
					window.location.href = "/dashboard";
				}
			},
			error: function (err) {
				if(err.status == 404){
					mostrarAlerta("error", "Credenciales incorrectas");
					return;
				}
					mostrarAlerta("error", "Ocurrio un error inesperado");
			},
		});
	} else {
		mostrarAlerta("error", "Complete los campos");
	}
}
