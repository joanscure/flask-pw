var data = [];
var products = [];

$(document).ready(function () {
	getAllData();
});

function getAllData() {
	$.ajax({
		url: "get-all-products",
		method: "GET",
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			products = respuesta;
			// add items to select element
			let select = $("#form").find('select[name="producto"]');
			select.empty();
			respuesta.forEach((element) => {
				select.append(`<option value="${element.id}">${element.nombre}</option>`);
			});

		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}
function validateForm(){
	let producto_id = $("#form").find('select[name="producto"]').val();
	let cantidad = $("#cantidad").val();
	if(producto_id == "" || cantidad == ""){
		mostrarAlerta("error", "Debe completar todos los campos");
		return false;
	}
	return true;
}

function addProductToTableHtml() {
	if(!validateForm()){
		return;
	}

	let producto_id = $("#form").find('select[name="producto"]').val();
	let producto = products.find((element) => {
		return element.id == producto_id;
	});

	let item = {
		id: producto.id,
		nombre: producto.nombre,
		precio: producto.precio,
		cantidad: $("#cantidad").val(),
		subtotal: $("#cantidad").val() * producto.precio,
	}

	data.push(item);
	let table = $("#table-body");
	table.empty();
	fillAllData();
	$("#cantidad").val('1');
}

function fillAllData() {

	let table = $("#table-body");
	table.empty();

	let total = 0;
	data.forEach((element) => {
		total = total + element.subtotal;
		table.append(`<tr>
			<td>${element.nombre}</td>
			<td>${element.precio}</td>
			<td>${element.cantidad}</td>
			<td>${element.subtotal}</td>
			<td>
				<button class="btn btn-danger" onclick="deleteItem(${element.id})">Eliminar</button>
			</td>
		</tr>`);
	});
	console.log('total :>> ', total);
 $("#total").text("S/ " + total);
}

function deleteItem(id) {
	let index = data.findIndex((element) => {
		return element.id == id;
	});

	data.splice(index, 1);
	fillAllData();
}

function sendData(eliminar = null) {
	if (!validateForm() && eliminar === null) {
		return;
	}
	let data = getFormData("product");
	data['eliminar'] = eliminar;


	$.ajax({
		url: "product",
		method: "POST",
		data: data,
		cache: false,
		dataType: "json",
		success: function (respuesta) {
			mostrarAlerta("success", "Se ejecuto correctamente");
			getAllData();
			$("#modal").modal("hide");
		},
		error: function () {
			mostrarAlerta("error", "Ocurrio un error inesperado");
		},
	});
}
function validateSale(){
	if(data.length == 0){
		mostrarAlerta('error', 'Debe agregar productos');
		return false;
	}

	if($("#fecha").val() == ""){
		mostrarAlerta('error', 'Debe ingresar una fecha');
		return false;
	}

	return true;
}

function clearSale(){
	if(!validateSale()){
		return;
	}
	data = [];
	fillAllData();
	$("#total").val('S/ 0');
	$("#cantidad").val('1');
	mostrarAlerta('success', 'Se ha registrados correctamente');
}
