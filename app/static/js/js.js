const mostrarAlerta = function (type, mensaje) {
  Swal.fire({
    icon: type,
    title: "SISTEMA GESTION ONG",
    text: mensaje,
  });
};
const getFormData = (form)=>{
   var config = {};
    $(`#${form} input`).each(function () {
     config[this.name] = this.value;
    });
  return config;
}
