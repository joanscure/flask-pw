import os
from app.main._init_ import main

from flask import (render_template,
                   redirect,
                   request, session)
from app.models.usuario import Usuario
from app.models.producto import Producto
from app.models.venta import Venta
from app.models.cliente import Cliente
from app.models.tipodocumento import TipoDocumento
from app.models.documentocontable import DocumentoContable
from app._init_ import db
from flask import abort

import json

@main.route("/login", methods=["POST"])
def validarLogin():
    nombre = request.form.get("nombre")
    clave = request.form.get("clave")
    usuarioDB = Usuario.query.filter_by(nombre=nombre).first()
    if usuarioDB and usuarioDB.clave == clave:
        session['usuario'] = usuarioDB.nombre
        return usuarioDB.to_full_json()

    return abort(404)


@main.route("/", methods=["GET"])
def login():
    return render_template("login.html")


@main.route("/dashboard", methods=["GET"])
def dashboard():
    products = Producto.query.all()
    ventas = Venta.query.all()
    clientes = Cliente.query.all()

    total_products = len(products)
    total_ventas = len(ventas)
    total_clientes = len(clientes)

    return render_template("dashboard.html", products=products, total_products=total_products, total_ventas=total_ventas, clientes=clientes, total_clientes=total_clientes)


@main.route("/inicio", methods=["GET"])
def inicio():
    return render_template("inicio.html")


@main.route("/products", methods=['GET'])
def producto():
    return render_template("product.html")


@main.route("/get-all-products", methods=['GET'])
def allproducto():
    products = Producto.query.all()
    list = []
    for item in products:
        list.append(item.to_full_json())
    resultado = json.dumps(list)
    return resultado;


@main.route("/product", methods=["POST"])
def opProduct():
    if request.method == 'POST':
        eliminar = request.form.get("eliminar")
        id = request.form.get("id")
        if not eliminar or eliminar == "":
            if not id or id == "":
                producto = Producto(request.form)
                db.session.add(producto)
            else:
                producto = Producto.query.filter_by(
                    id=id).first()
                producto.nombre = request.form.get(
                    "nombre", default="", type=str)
                producto.precio = request.form.get(
                    "precio", default=0, type=int)
                producto.stock = request.form.get(
                    "stock", default=0, type=int)
                producto.stock_minimo = request.form.get(
                    "stock_minimo", default="", type=int)
        else:
            producto = Producto.query.filter_by(id=eliminar).first()
            db.session.delete(producto)
        db.session.commit()
    return producto.to_full_json();


@main.route("/user", methods=['GET'])
def user():
    return render_template("usuario.html")


@main.route("/get-all-users", methods=['GET'])
def alluser():
    users = Usuario.query.all()
    list = []
    for item in users:
        list.append(item.to_full_json())
    resultado = json.dumps(list)
    return resultado;


@main.route("/user", methods=["POST"])
def getUsuario():
    if request.method == 'POST':
        eliminar = request.form.get("eliminar")
        id = request.form.get("id")
        if not eliminar or eliminar == "":
            if not id or id == "":
                usuario = Usuario(request.form)
                db.session.add(usuario)
                message = "Registro exitoso"
            else:
                usuario = Usuario.query.filter_by(
                    id=id).first()
                usuario.usuario = request.form.get(
                    "usuario", default="", type=str)
                usuario.clave = request.form.get(
                    "clave", default="", type=str)
                usuario.email = request.form.get(
                    "email", default="", type=str)
        else:
            usuario = Usuario.query.filter_by(id=eliminar).first()
            db.session.delete(usuario)
        db.session.commit()
    return usuario.to_full_json();



@main.route("/customer", methods=['GET'])
def customer():
    tipo_documentos = TipoDocumento.query.all()
    return render_template("customer.html", tipo_documentos=tipo_documentos)


@main.route("/get-all-customer", methods=['GET'])
def allclients():
    products = Cliente.query.all()
    list = []
    for item in products:
        list.append(item.to_full_json())
    results = json.dumps(list)
    return results;


@main.route("/customer", methods=["POST"])
def opClients():
    if request.method == 'POST':
        eliminar = request.form.get("eliminar")
        id = request.form.get("id")
        if not eliminar or eliminar == "":
            if not id or id == "":
                client = Cliente(request.form)
                db.session.add(client)
            else:
                client = Cliente.query.filter_by(
                    id=id).first()
                client.documento_identidad = request.form.get(
                    "documento_identidad", default="", type=str)
                client.nombre_completo = request.form.get(
                    "nombre_completo", default=0, type=str)
                client.email = request.form.get(
                    "email", default=0, type=str)
                client.tipo_documento_id = request.form.get(
                    "tipo_documento_id", default="", type=str)
                client.direccion = request.form.get(
                    "direccion", default="", type=str)
        else:
            client = Cliente.query.filter_by(id=eliminar).first()
            db.session.delete(client)
        db.session.commit()
    return client.to_full_json();


@main.route("/my-profile")
def myprofile():
    return render_template('profile.html');

@main.route("/venta")
def venta():
    documentos_contables = DocumentoContable.query.all()
    clientes = Cliente.query.all()
    return render_template('venta.html', documentos_contables=documentos_contables, clientes=clientes)

