from app._init_ import db
from sqlalchemy import Sequence


class Cliente(db.Model):
    __tablename__ = 'cliente'
    id = db.Column(db.Integer, primary_key=True)
    nombre_completo = db.Column(db.String(20), unique=False, nullable=False)
    documento_identidad = db.Column(db.Integer, unique=False, nullable=False)
    email = db.Column(db.String(50), unique=False, nullable=False)
    tipo_documento_id = db.Column(db.Integer, unique=False, nullable=False)
    direccion = db.Column(db.String(100), unique=False, nullable=False)

    def __init__(self, form):
        if form.get('id') != "":
            self.id = form.get('id')

        self.documento_identidad = form.get('documento_identidad')
        self.nombre_completo = form.get('nombre_completo')
        self.email = form.get('email')
        self.tipo_documento_id = form.get('tipo_documento_id')
        self.direccion = form.get('direccion')

    def to_full_json(self):
        json = {
            'status': 200,
            'id': self.id,
            'documento_identidad': self.documento_identidad,
            'nombre_completo': self.nombre_completo,
            'tipo_documento_id': self.tipo_documento_id,
            'direccion': self.direccion,
            'email': self.email
        }
        return json
