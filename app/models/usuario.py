from app._init_ import db
from sqlalchemy import Sequence


class Usuario(db.Model):
    __tablename__ = 'usuario'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), unique=False, nullable=False)
    clave = db.Column(db.String(10), unique=False, nullable=False)
    email = db.Column(db.String(100), unique=False, nullable=False)

    def __init__(self, form):

        if form.get('id') != "":
            self.id = form.get('id')

        self.nombre = form.get('nombre')
        self.clave = form.get('clave')
        self.email = form.get('email')

    def to_full_json(self):
        json_usuario = {
            'status': 200,
            'id': self.id,
            'nombre': self.nombre,
            'clave': self.clave,
            'email': self.email
        }
        return json_usuario
