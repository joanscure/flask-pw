from app._init_ import db
from sqlalchemy import Sequence


class TipoDocumento(db.Model):
    __tablename__ = 'tipo_documento'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(20), unique=False, nullable=False)

    def __init__(self, form):
        if form.get('id') != "":
            self.id = form.get('id')

        self.nombre = form.get('nombre')

    def to_full_json(self):
        json = {
            'status': 200,
            'id': self.id,
            'nombre': self.nombre,
        }
        return json
