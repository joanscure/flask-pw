from app._init_ import db
from sqlalchemy import Sequence


class VentaDetalle(db.Model):
    __tablename__ = 'venta_detalle'
    id = db.Column(db.Integer, primary_key=True)
    venta_id = db.Column(db.Integer, db.ForeignKey('venta.id'))
    producto_id = db.Column(db.Integer, db.ForeignKey('producto.id'))
    cantidad = db.Column(db.Integer)
    precio = db.Column(db.Float)
    subtotal = db.Column(db.Float)

    def __init__(self, form):
        if form.get('id') != "":
            self.id = form.get('id')

        self.venta_id = form.get('venta_id')
        self.producto_id = form.get('producto_id')
        self.cantidad = form.get('cantidad')
        self.precio = form.get('precio')
        self.subtotal = form.get('subtotal')

    def to_full_json(self):
        json = {
            'status': 200,
            'id': self.id,
            'venta_id': self.venta_id,
            'producto_id': self.producto_id,
            'precio': self.precio,
            'subtotal': self.subtotal,
            'cantidad': self.cantidad
        }
        return json
