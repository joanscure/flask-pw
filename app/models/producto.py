from app._init_ import db
from sqlalchemy import Sequence


class Producto(db.Model):
    __tablename__ = 'producto'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), unique=False, nullable=False)
    precio = db.Column(db.Float, unique=False, nullable=False)
    stock = db.Column(db.Integer, unique=False, nullable=False)
    stock_minimo = db.Column(db.Integer, unique=False, nullable=False)

    def __init__(self, form):
        if form.get('id') != "":
            self.id = form.get('id')

        self.nombre = form.get('nombre')
        self.precio = form.get('precio')
        self.stock = form.get('stock')
        self.stock_minimo = form.get('stock_minimo')

    def to_full_json(self):
        json = {
            'status': 200,
            'id': self.id,
            'nombre': self.nombre,
            'precio': self.precio,
            'stock_minimo': self.stock_minimo,
            'stock': self.stock
        }
        return json
