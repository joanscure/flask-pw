from app._init_ import db
from sqlalchemy import Sequence


class Venta(db.Model):
    __tablename__ = 'venta'
    id = db.Column(db.Integer, primary_key=True)
    cliente_id = db.Column(db.Integer, unique=False, nullable=False)
    fecha = db.Column(db.String(10), unique=False, nullable=False)
    documento_contable_id = db.Column(db.Integer, unique=False, nullable=False)
    total = db.Column(db.Float, unique=False, nullable=False)

    def __init__(self, form):
        if form.get('id') != "":
            self.id = form.get('id')

        self.fecha = form.get('fecha')
        self.cliente_id = form.get('cliente_id')
        self.documento_contable_id = form.get('documento_contable_id')
        self.total = form.get('total')

    def to_full_json(self):
        json = {
            'status': 200,
            'id': self.id,
            'fecha': self.fecha,
            'cliente_id': self.cliente_id,
            'total': self.total,
            'documento_contable_id': self.documento_contable_id
        }
        return json
