create DATABASE venta;

use venta;

CREATE TABLE usuario (
    id SERIAL,
    nombre varchar,
	clave varchar,
	email varchar
);

CREATE TABLE cliente (
    id SERIAL,
    nombre_completo varchar,
	documento_identidad varchar,
	email varchar,
	tipo_documento_id int,
	direccion varchar
);

CREATE TABLE productos (
    id SERIAL,
    nombre varchar,
	precio numeric(10,2),
	stock int,
	stock_minimo int
);

CREATE TABLE venta (
    id SERIAL,
    fecha varchar,
	documento_contable_id int,
	cliente_id int,
	total int
);

CREATE TABLE tipo_documento (
    id SERIAL,
    nombre varchar
);

CREATE TABLE documento_contable (
    id SERIAL,
    nombre varchar
);

CREATE TABLE venta_detalle (
    id SERIAL,
    venta_id int,
	producto_id int,
	precio numeric(10,2),
	cantidad int,
	subtotal numeric(10,2)
);
CREATE TABLE venta_pago (
    id SERIAL,
    venta_id int,
	monto_recibido numeric(10,2),
	vuelto numeric(10,2)
);

INSERT INTO public.tipo_documento(
	nombre)
VALUES ('DNI');
INSERT INTO public.tipo_documento(
	nombre)
VALUES ('RUC');
INSERT INTO public.tipo_documento(
	nombre)
VALUES ('OTRO');
INSERT INTO public.documento_contable(
	nombre)
VALUES ( 'BOLETA');
